/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.SQLException;
import BusinessLogic.*;
import java.sql.DriverManager;
/**
 *
 * @author COMPUTER
 */
public class ConnectionManager {
    private Connection conn = null;
    
    // 3 constant strings below can be change up to your client.
    final String connectString = "jdbc:sqlserver://127.0.0.1;sqlInstanceName=SQLEXPRESS;databaseName=SalesManagement";
    final String userName = "sa";
    final String Password = "1";
    
    public void DisplayError (SQLException ex) {
        // Show Error messages to screen
        AppPublic.frmMain.ShowErrorMessagePane(ex.getMessage(), ex.getSQLState(), ex.getErrorCode());
    }
    public void Connect () {
        // Connect to DB
        try {
           conn = DriverManager.getConnection(connectString, userName, Password);
        } catch (SQLException ex) {
            DisplayError(ex);
        }
        // Show the response to form main
        if(conn != null)
            AppPublic.frmMain.ShowConnectResponse(true);
        else
            AppPublic.frmMain.ShowConnectResponse(false);
    }
}
